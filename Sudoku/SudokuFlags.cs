﻿using System;

namespace Sudoku
{
    class SudokuFlags : ICloneable
    {
        public const byte ValidFlag = 0x1;

        public const byte SettedFlag = 0x2;

        public const byte TrackedFlag = 0x4;

        public const byte ConstFlag = 0x8;

        private const byte Mb = 255;

        private SudokuFlags(byte flag)
        {
            _flag = flag;
        }

        public SudokuFlags()
        {
            _flag = ValidFlag;
        }

        public void Reset()
        {
            _flag = ValidFlag;
        }

        private byte _flag;

        public bool IsValid
        {
            get { return (_flag & ValidFlag) != 0; }
            set
            {
                if (value) _flag |= ValidFlag;
                else
                    _flag &= (byte)(ValidFlag ^ Mb);
            }
        }

        public bool IsSetted
        {
            get { return (_flag & SettedFlag) != 0; }
            set
            {
                if (value) _flag |= SettedFlag;
                else
                    _flag &= (byte)(SettedFlag ^ Mb);
            }
        }

        public bool IsTracked
        {
            get { return (_flag & TrackedFlag) != 0; }
            set
            {
                if (value) _flag |= TrackedFlag;
                else
                    _flag &= (byte)(TrackedFlag ^ Mb);
            }
        }

        public bool IsConst
        {
            get { return (_flag & ConstFlag) != 0; }
            set
            {
                if (value) _flag |= ConstFlag;
                else
                    _flag &= (byte)(ConstFlag ^ Mb);
            }
        }

        public object Clone()
        {
            return new SudokuFlags(_flag);
        }
    }
}
