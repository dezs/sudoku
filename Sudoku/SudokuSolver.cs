﻿using System.Globalization;
using System.Text;

namespace Sudoku
{
    class SudokuSolver
    {
        public SudokuField Field { get; set; }
    
        private SudokuField _fieldBackup;
        private readonly SudokuGraf _graf;

        /// <summary>
        /// Find not-set sell on field with minimal possible valid variant count
        /// </summary>
        /// <param name="field">Sudoku field</param>
        /// <returns>Coords on field</returns>
        private Coord MinVariantCount(SudokuField field)
        {
            var c = new Coord(0, 0);
            for (int i = 0; i < field.Height; i++)
            {
                for (int j = 0; j < field.Width; j++)
                {
                    if (field[i, j].SettedFlag)
                        continue;
                    if ((field[i, j].VariantCount < field[c.X, c.Y].VariantCount) || (field[c.X, c.Y].SettedFlag))
                    {
                        c.X = i;
                        c.Y = j;
                    }
                }
            }
            return c;
        }

        //wtf
        private void CheckSell(int i,int j)
        {
            Field[i,j].ResetValue();
            foreach (var c in _graf[i, j])
            {
                if (Field[c.X, c.Y].SettedFlag)
                {
                    Field[i, j].ExcludeValue(Field[c.X, c.Y].ViewedValue);
                }
            }
        }


        private void SolveUniquely(SudokuField field)
        {
            bool flag;
            do
            {
                flag = false;
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        if (!field[i, j].SettedFlag)
                        {
                            if (field[i, j].VariantCount == 1)
                            {
                                Set(i, j, field[i, j].GetPossibleValues[0], field);
                                flag = true;
                            }
                            else
                            {
                                for (int k = 0; k < 3; k++)
                                {
                                    int v = field[i, j].Value;
                                    foreach (var coord in _graf[i, j, k])
                                    {
                                        v = BitWorker.GetResultValue(v, field[coord.X, coord.Y].Value);
                                    }
                                    if (BitWorker.Count[v] == 1)
                                    {
                                        Set(i, j, BitWorker.GetPossibleValues(v)[0], field, false);
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } while (flag);
        }


        private bool SolveAmbiguous(Coord coord, SudokuField field)
        {
            foreach (var i in field[coord.X,coord.Y].GetPossibleValues)
            {
                var sf = field.Clone() as SudokuField;
                Set(coord.X, coord.Y, i, sf);
                SolveUniquely(sf);
                if (sf.CheckValid(_graf))
                {
                    if (CheckSolve(sf))
                    {
                        Field = (SudokuField) sf.Clone();
                        return true;
                    }
                    if (SolveAmbiguous(MinVariantCount(sf), sf))
                        return true;
                }

            }
            return false;
        }

        public SudokuSolver()
        {
            Field = new SudokuField();
            _fieldBackup = new SudokuField();
            _graf = new SudokuGraf();
        }

        public void RestoreBackup()
        {
            Field = _fieldBackup.Clone() as SudokuField;
        }

        public bool CreateBackup()
        {
            if (CheckValid())
            {
                _fieldBackup = Field.Clone() as SudokuField;
                return true;
            }
            return false;
        }

        public int TestSolve()
        {
            int completety = 1;
            //SudokuField sudokuField = Field.Clone() as SudokuField;
            do
            {
                if (!CheckValid())
                {
                    //Field = sudokuField.Clone() as SudokuField;
                    return -1;
                }
                SolveUniquely(Field);
                if (!CheckSolve())
                {
                    Coord c = MinVariantCount(Field);
                    completety +=Field[c.X,c.Y].VariantCount;
                    SolveAmbiguous(c, Field);
                }
            } while (!CheckSolve());
            return completety;
        }

        public void Set(int x, int y, byte v, SudokuField field = null, bool tracked = false)
        {
            if (field == null)
                field = Field;
            if (field[x,y].SettedFlag)
            {
                Unset(x, y, field);
            }
            if ((v < 1) || (v > 9))
                return;
            
            if (field[x, y].SetSell(v, tracked))
                foreach (var c in _graf[x, y])
                {
                    Field[c.X, c.Y].ExcludeValue(v);
                }
        }

        public void Unset(int x, int y, SudokuField field)
        {
            field[x, y].ResetValue();
            foreach (var c in _graf[x, y])
            {
                if (field[c.X, c.Y].SettedFlag)
                {
                    field[x,y].ExcludeValue(field[c.X, c.Y].ViewedValue);
                }
                else
                {
                    CheckSell(c.X,c.Y);
                }
            }
        }

        public bool CheckValid()
        {
            return Field.CheckValid(_graf);
        }

        public bool CheckSolve(SudokuField field = null)
        {
            if (field == null)
                field = Field;
            foreach (SudokuSell sell in field)
            {
                if ((!sell.SettedFlag) || (!sell.ValidFlag))
                    return false;
            }
            return true;
        }

        public void ClearTrack()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (Field[i, j].TrackFlag)
                        Unset(i, j, Field);
                }
            }
        }

        public void AddTrackToOrigin()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (Field[i, j].TrackFlag)
                        Field[i, j].TrackFlag = false;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (Field[i, j].SettedFlag)
                    {
                        sb.Append(Field[i, j].ValidFlag ? " " : "!");
                        sb.Append(Field[i, j].GetViewString());
                        sb.Append(" ");
                    }
                    else
                    {
                        sb.Append(" _ ");
                    }
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }        

        public SudokuSell this[int i, int j]
        {
            get { return Field[i, j]; }
        }
    }
}
