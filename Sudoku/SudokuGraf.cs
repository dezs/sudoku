﻿using System.Collections.Generic;

namespace Sudoku
{
    struct Coord
    {
        public Coord(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X,Y;
    }
    class SudokuGraf
    {
        private readonly List<Coord>[,,] _graf;

        private readonly List<int>[] _grafSv = new List<int>[3];

        public SudokuGraf()
        {
            _graf = new List<Coord>[9,9,3];
            CreateGraf();
        }

        private void CreateGraf()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    for (int k = 0; k < 3; k++)
                    {
                        _graf[i, j, k] = new List<Coord>();
                    }
                    for (int k = 0; k < 9; k++)
                    {
                        if (i != k)
                            _graf[i, j, 0].Add(new Coord(k, j));
                        if (j != k)
                            _graf[i, j, 1].Add(new Coord(i, k));
                    }
                    int cX = (i / 3) * 3 + 1;
                    int cY = (j / 3) * 3 + 1;
                    for (int ci = -1; ci < 2; ci++)
                    {
                        for (int cj = -1; cj < 2; cj++)
                        {
                            if (((ci + cX) != i) || ((cj + cY) != j) && ((cX + ci) >= 0) && ((cY + cj) >= 0))
                                _graf[i, j, 2].Add(new Coord(ci + cX, cj + cY));
                        }
                    }
                }
            }
            for (int i = 0; i < 3; i++)
            {
                _grafSv[i] = new List<int>();
                for (int j = 0; j < 8; j++)
                {
                    _grafSv[i].Add(j + i*8);
                }
            }
        }

        public List<Coord> this[int i, int j, int k = 4]
        {
            get
            {
                if ((k >= 0) && (k < 3))
                    return _graf[i, j, k];
                var t = new List<Coord>();
                t.AddRange(_graf[i, j, 0]);
                t.AddRange(_graf[i, j, 1]);
                t.AddRange(_graf[i, j, 2]);
                return t;
            }
        }
    }
}
