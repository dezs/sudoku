﻿using System;

namespace Sudoku
{
    public class SudokuSell : IViewedSell, ICloneable
    {

        private int _value;

        private readonly SudokuFlags _flags;

        private SudokuSell(int value, SudokuFlags flags, byte viewedValue)
        {
            _value = value;
            _flags = flags.Clone() as SudokuFlags;
            ViewedValue = viewedValue;
        }

        public SudokuSell()
        {
            _value = BitWorker.MaxValue;
            _flags = new SudokuFlags();
        }

        public bool SetSell(byte v, bool tracked = false)
        {
            ViewedValue = v;
            TrackFlag = tracked;
            ValidFlag = BitWorker.CheckBit(_value, v);
            _value = BitWorker.Pow2[v - 1];
            SettedFlag = true;
            return ValidFlag;
        }

        public void ResetValue()
        {
            _value = BitWorker.MaxValue;
            _flags.Reset();
        }

        public bool ExcludeValue(int v)
        {
            return BitWorker.ExcludeBit(ref _value, v);
        }

        public byte[] GetPossibleValues
        {
            get { return BitWorker.GetPossibleValues(Value); }
        }
        
        public int Value { get { return _value; } }

        public byte ViewedValue { get; protected set; }

        public bool TrackFlag
        {
            get { return _flags.IsTracked; }
            set { _flags.IsTracked = value; }
        }

        public bool ValidFlag
        {
            get { return _flags.IsValid; }
            set { _flags.IsValid = value; }
        }

        public bool SettedFlag
        {
            get { return _flags.IsSetted; }
            protected set { _flags.IsSetted = value; }
        }

        public bool ConstFlag
        {
            get { return _flags.IsConst; }
            protected set { _flags.IsConst = value; }
        }

        public int VariantCount { get { return BitWorker.Count[Value]; } }

        public string GetViewString()
        {
            if (SettedFlag)
            {
                return ViewedValue.ToString();
            }
            return " ";
        }
        
        public object Clone()
        {
            return new SudokuSell(_value, (SudokuFlags) _flags.Clone(), ViewedValue);
        }

    }
}
