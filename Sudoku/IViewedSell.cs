﻿namespace Sudoku
{
    public interface IViewedSell
    {
        /// <summary>
        /// Value to display
        /// </summary>
        byte ViewedValue { get; }

        /// <summary>
        /// Flag indicate is this sell "tracked"
        /// </summary>
        bool TrackFlag { get; }

        /// <summary>
        /// Is sell valid
        /// </summary>
        bool ValidFlag { get; }

        /// <summary>
        /// Is value set
        /// </summary>
        bool SettedFlag { get; }

        /// <summary>
        /// Const mean user can not modify this sell
        /// </summary>
        bool ConstFlag { get; }

        string GetViewString();
    }
}
