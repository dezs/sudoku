﻿using System;
using System.Collections.Generic;

namespace Sudoku
{
    public static class BitWorker
    {
        //Magic
        public static short MaxValue = 511;

        private static readonly short[] Bytearr;

        private static short[] _arr;

        static BitWorker()
        {
            FillArr();
            Bytearr = new short[MaxValue + 1];
            for (int i = 0; i < (MaxValue + 1); i++)
            {
                Bytearr[i] = BitCount(i);
            }
        }

        /// <summary>
        /// Array of degrees of 2
        /// </summary>
        private static void FillArr()
        {
            var l = new List<short>();
            short v = 1;
            do
            {
                l.Add(v);
                v <<= 1;
            } while (v < MaxValue);
            _arr = l.ToArray();
        }

        /// <summary>
        /// Returns count of 1 in binary representation of variable c
        /// </summary>
        /// <param name="c">Analyzed variable</param>
        /// <returns>Bit count</returns>
        private static Int16 BitCount(int c)
        {
            Int16 count = 0;
            for (int i = 8; i >= 0; i--)
            {
                if ((c >= _arr[i]))
                {
                    c -= _arr[i];
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Magic!
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetPossibleValues(int value)
        {
            var list = new List<byte>();
            for (int i = 8; i >= 0; i--)
            {
                if ((value >= _arr[i]))
                {
                    value -= _arr[i];
                    list.Add((byte)(i + 1));
                }
            }
            return list.ToArray();
        }

        /// <summary>
        /// Magic!
        /// </summary>
        /// <param name="firstvalue"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetResultValue(int firstvalue, params int[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                firstvalue = firstvalue & (value[i] ^ MaxValue);
            }
            return firstvalue;
        }

        /// <summary>
        /// Check bit on position bitPosition in value 
        /// </summary>
        /// <param name="value">Checked value</param>
        /// <param name="bitPosition">Position of bit in value</param>
        /// <returns>True if bit on position equals 1, else false</returns>
        public static bool CheckBit(int value, int bitPosition)
        {
            return (value & (1 << (bitPosition - 1))) != 0;
        }

        /// <summary>
        /// Set to 0 bit on position bitPosition in value.
        /// </summary>
        /// <param name="value">Modified value</param>
        /// <param name="bitPosition">Position of bit in value</param>
        /// <returns>true if bit on position was 1, else false</returns>
        public static bool ExcludeBit(ref int value, int bitPosition)
        {
            bool ex = CheckBit(value, bitPosition);
            value = value & ((1 << (bitPosition - 1)) ^ MaxValue);
            return ex;
        }

        /// <summary>
        /// Magic
        /// </summary>
        public static Int16[] Count
        {
            get { return Bytearr; }
        }

        /// <summary>
        /// Magic
        /// </summary>
        public static short[] Pow2
        { get { return _arr; } }
    }
}
