﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace Sudoku
{
    class SudokuField:ICloneable,IEnumerable
    {
        private readonly SudokuSell[,] _field;

        public int Height
        {
            get { return _field.GetLength(0); }
        }

        public int Width
        {
            get { return _field.GetLength(1); }
        }

        private SudokuField(SudokuSell[,] f)
        {
            var h = f.GetLength(0);
            var w = f.GetLength(1);
            _field = new SudokuSell[w, h];
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    _field[i, j] = f[i, j].Clone() as SudokuSell;
                }
            }
        }

        public SudokuField(int height = 9, int widht = 9)
        {
            _field = new SudokuSell[height, widht];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < widht; j++)
                {
                    _field[i, j] = new SudokuSell();
                }
            }
        }

        public SudokuSell this[int i, int j]
        {
            get { return _field[i, j]; }
        }

        public bool CheckValid(SudokuGraf graf)
        {
            foreach (SudokuSell sell in _field)
            {
                if (sell.VariantCount == 0)
                    return false;
                if (!sell.ValidFlag)
                    return false;
                for (int i = 0; i < Width; i++)
                {
                    for (int j = 0; j < Height; j++)
                    {
                        for (int k = 0; k < 3; k++)
                        {
                            int v = _field[i, j].Value;
                            foreach (var coord in graf[i, j, k])
                            {
                                v |= _field[coord.X, coord.Y].Value;
                            }
                            if (v != BitWorker.MaxValue)
                                return false;
                        }
                    }
                }
            }
            return true;
        }

        public object Clone()
        {
            return new SudokuField(_field);
        }
        
        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < _field.GetLength(0); i++)
            {
                for (int j = 0; j < _field.GetLength(1); j++)
                {
                    yield return _field[i, j];
                }
            }
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    s.AppendFormat(" {0} ", _field[i, j].Value);
                }
                s.AppendLine();
            }
            return s.ToString();
        }
    }
}
