﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace Sudoku
{
    public partial class Form1 : Form
    {
        private readonly SudokuSolver _sf;
        public Form1()
        {
             InitializeComponent();
            _sf = new SudokuSolver();
            
            field.Rows.Add(9);
            
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    field.Rows[i].Cells[j].Value = "";
                    //field.Columns[i].
                }
            }
            richTextBox1.Text = _sf.ToString();
            
        }

        private void Button2Click(object sender, EventArgs e)
        {
            if (!_sf.CheckValid())
                MessageBox.Show("Error in solution!");
            else
                MessageBox.Show("no errors found");
        }
        private readonly Color[] _colors=new[]{Color.Black,Color.Red,Color.Blue};
        private void FillField()
        {
            richTextBox1.Text = _sf.ToString();
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    field.Rows[i].Cells[j].Value = _sf[i, j].GetViewString();
                    if (_sf[i, j].ValidFlag)
                        field.Rows[i].Cells[j].Style.ForeColor = _sf[i, j].TrackFlag ? _colors[2] : _colors[0];
                    else
                    {
                        field.Rows[i].Cells[j].Style.ForeColor = _colors[1];
                    }
                }
            }
        }

        private void FieldCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            byte newInteger;

            if (field.Rows[e.RowIndex].IsNewRow) { return; }
            if (!byte.TryParse(e.FormattedValue.ToString(),
                out newInteger) || newInteger < 0 || newInteger > 9)
            {
                FillField();
            }
            else
            {
                _sf.Set(e.RowIndex , e.ColumnIndex , newInteger, tracked:checkTrack.Checked);
                FillField();
            }
        }

        private void ButtonSolveClick(object sender, EventArgs e)
        {
            label1.Text = _sf.TestSolve().ToString();
            FillField();
        }

        private void ButtonClearClick(object sender, EventArgs e)
        {
            _sf.ClearTrack();
            FillField();
        }

        private void ButtonAddClick(object sender, EventArgs e)
        {
            _sf.AddTrackToOrigin();
            FillField();
        }

        private void ButtonFillClick(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    _sf.Unset(i, j,_sf.Field);
                }
            }
            _sf.Set(0, 2, 1);
            _sf.Set(0, 5, 6);
            _sf.Set(1, 1, 3);
            _sf.Set(1, 6, 7);
            _sf.Set(2, 0, 8);
            _sf.Set(2, 4, 5);
            _sf.Set(2, 4, 7);
            _sf.Set(2, 7, 4);
            _sf.Set(3, 1, 4);
            _sf.Set(3, 7, 3);
            _sf.Set(4, 0, 1);
            _sf.Set(4, 3, 9);
            _sf.Set(5, 1, 2);
            _sf.Set(5, 2, 5);
            _sf.Set(5, 3, 1);
            _sf.Set(5, 5, 8);
            _sf.Set(6, 1, 1);
            _sf.Set(6, 5, 5);
            _sf.Set(6, 6, 3);
            _sf.Set(6, 7, 9);
            _sf.Set(7, 2, 2);
            _sf.Set(8, 2, 3);
            _sf.Set(8, 3, 2);
            _sf.Set(8, 4, 9);
            _sf.Set(8, 7, 5);
            /*
            _sf.Set(0, 0, 2);
            _sf.Set(0, 5, 6);
            _sf.Set(0, 7, 9);
            _sf.Set(1, 3, 3);
            _sf.Set(1, 4, 8);
            _sf.Set(1, 7, 4);
            _sf.Set(2, 1, 7);
            _sf.Set(2, 3, 4);
            _sf.Set(2, 8, 8);
            _sf.Set(3, 1, 6);
            _sf.Set(3, 2, 8);
            _sf.Set(3, 4, 3);
            _sf.Set(4, 0, 1);
            _sf.Set(4, 3, 5);
            _sf.Set(4, 5, 8);
            _sf.Set(4, 8, 2);
            _sf.Set(5, 4, 7);
            _sf.Set(5, 6, 1);
            _sf.Set(5, 7, 8);
            _sf.Set(6, 0, 7);
            _sf.Set(6, 5, 3);
            _sf.Set(6, 7, 6);
            _sf.Set(7, 1, 9);
            _sf.Set(7, 4, 2);
            _sf.Set(7, 5, 5);
            _sf.Set(8, 1, 5);
            _sf.Set(8, 3, 7);
            _sf.Set(8, 8, 4);*/
            FillField();
        }


        private void ButtonBackupClick(object sender, EventArgs e)
        {
            _sf.CreateBackup();
        }

        private void ButtonRestoreBackupClick(object sender, EventArgs e)
        {
            _sf.RestoreBackup();
            FillField();
        }
    }
}
